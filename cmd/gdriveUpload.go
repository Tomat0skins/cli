/**
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"github.com/spf13/cobra"
)

// gdriveUploadCmd represents the gdriveUpload command
var gdriveUploadCmd = &cobra.Command{
	Use:   "gdrive:upload",
	Short: "Google Drive Upload (Experimental)",
	Run: func(cmd *cobra.Command, args []string) {
		utils.GoogleDriveExample()
	},
}

func init() {
	rootCmd.AddCommand(gdriveUploadCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gdriveUploadCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// gdriveUploadCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
