/**
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"text/template"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// Verbose
var Verbose bool
var version = "1.0.8"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Version: version,
	Use:     "xpol",
	Short:   "XPOL CLI by Altercode",
	Long:    `This is a GO Command Line Interface made by Altercode`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	/* Run: func(cmd *cobra.Command, args []string) {

	}, */

}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		utils.Err(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	cobra.AddTemplateFuncs(template.FuncMap{
		"FG_Bold":      color.New(color.Bold).SprintFunc(),
		"FG_Underline": color.New(color.Underline).SprintFunc(),
		"FG_Green":     color.New(color.FgGreen).SprintFunc(),
		"FG_Yellow":    color.New(color.FgYellow).SprintFunc(),
		"FG_Cyan":      color.New(color.FgCyan).SprintFunc(),
		"FG_Red":       color.New(color.FgRed).SprintFunc(),
	})

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")

	rootCmd.SetHelpTemplate(`{{with (or .Long .Short)}}{{. | trimTrailingWhitespaces  | FG_Bold }}

{{end}}{{if or .Runnable .HasSubCommands}}{{.UsageString}}{{end}}`)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetConfigType("json")
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
		viper.AddConfigPath(filepath.Dir(cfgFile))
	} else {
		// Find home directory.
		home := utils.XPOLDir()
		viper.SetConfigName("config")
		viper.AddConfigPath(home)

	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	err := viper.ReadInConfig()
	if err == nil {
		if Verbose {
			utils.Info(rootCmd.CommandPath())
			utils.Info("Using config file: " + viper.ConfigFileUsed())
		}
	} else {
		utils.Err(err)
		utils.Warn("Run init command to fix this!")
	}
}

// YesNoParameters
type YesNoParameters struct {
	YesNoLabel string `default:"Select[Yes/No]"`
}

// YesNo
func YesNo(prm YesNoParameters) bool {
	prompt := promptui.Select{
		Label: prm.YesNoLabel,
		Items: []string{"Yes", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}

// ExecuteCommand
func ExecuteCommand(input string) (*exec.Cmd, error) {
	c := exec.Command("cmd", "/C", input)
	c.Env = os.Environ()
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	err := c.Run()

	if err != nil {
		log.Fatalf("XPOLCLI failed with %s\n", err)
	}
	return c, err
}

// ExecutePowershell
func ExecutePowershell(input string) (*exec.Cmd, error) {
	c := exec.Command("PowerShell", input)
	c.Env = os.Environ()
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	err := c.Run()
	if err != nil {
		log.Fatalf("XPOLCLI failed with %s\n", err)
	}
	return c, err
}

// If
func If[T any](cond bool, vtrue, vfalse T) T {
	if cond {
		return vtrue
	}
	return vfalse
}

func OpenBrowserUrl(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		utils.Warn("unsupported platform; no browser")
	}
	if err != nil {
		utils.Err(err)
	}
}

func viperSetSave(key string, value interface{}) error {
	viper.Set(key, value)
	err := viper.WriteConfig()
	if err != nil {
		return err
	}
	_ = viper.ReadInConfig()
	return nil
}
