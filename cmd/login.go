/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"github.com/spf13/cobra"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Login for graphql features",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Warn("Login:")
		var usr = utils.PromptLogin("Username", false)
		var psw = utils.PromptLogin("Password", true)
		validCredentials, userLogin, err := utils.ValidateLogin(usr, psw)

		if err != nil {
			utils.Err(err)
			return
		}

		if validCredentials {
			jwtString, err := utils.GenerateJWT(userLogin)
			if err != nil {
				utils.Err(err)
				return
			}
			utils.StoreJWT(jwtString)
			utils.Succ("Login succeed!")
		}

	},
}

func init() {
	rootCmd.AddCommand(loginCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// loginCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// loginCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
