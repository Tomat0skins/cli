/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os/exec"
	"strconv"
)

var setDir string
var kill bool

// astroserverCmd represents the astroserver command
var astroserverCmd = &cobra.Command{
	Use:   "astroserver",
	Short: "Starts the astroneer server",
	Run: func(cmd *cobra.Command, args []string) {
		if setDir != "" {
			err := viperSetSave("astroneer_path", setDir)
			if err != nil {
				utils.Err(err)
			} else {
				utils.Info("astroneer_path is now " + setDir)
			}
		}
		var serverPid = viper.GetInt("astroneer_pid")

		if kill {
			if serverPid != 0 {
				kill := exec.Command("taskkill", "/T", "/F", "/PID", strconv.Itoa(serverPid))
				err := kill.Run()
				if err != nil {
					utils.Err(err)
				}
				utils.Succ("AstroNeer Server closed")
				_ = viperSetSave("astroneer_pid", 0)
			} else {
				utils.Warn("AstroNeer server is not running")
			}
			return
		}
		var serverPath = viper.GetString("astroneer_path")
		if serverPath != "" {
			if serverPid != 0 {
				utils.Info("AstroNeer Server is already running on PID: " + strconv.Itoa(serverPid))
				return
			}
			cmd := exec.Command(serverPath + "AstroServer.exe")
			utils.Info("Started " + serverPath + "AstroServer.exe")
			err := cmd.Start()
			if err != nil {
				utils.Err(err)
				return
			}
			pid := cmd.Process.Pid
			_ = viperSetSave("astroneer_pid", pid)
			// use goroutine waiting, manage process
			// this is important, otherwise the process becomes in S mode
			go func() {
				err = cmd.Wait()
				fmt.Printf("Command finished with error: %v", err)
			}()

		} else {
			utils.Warn("No server path configured, use --set-dir flag")
		}
	},
	PostRun: func(cmd *cobra.Command, args []string) {

	},
}

func init() {
	rootCmd.AddCommand(astroserverCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// astroserverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	astroserverCmd.Flags().StringVarP(&setDir, "set-dir", "d", "", "Set astroneer server path")
	astroserverCmd.Flags().BoolVarP(&kill, "kill", "k", false, "Turn off the server")
}
