/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"github.com/spf13/cobra"
)

var CliGitRepoURL = "https://gitlab.com/altercode/cli"

// gitRepoCmd represents the gitRepo command
var gitRepoCmd = &cobra.Command{
	Use:   "git-repo",
	Short: "Open the git repo",
	Run: func(cmd *cobra.Command, args []string) {
		OpenBrowserUrl(CliGitRepoURL)
	},
}

func init() {
	rootCmd.AddCommand(gitRepoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gitRepoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// gitRepoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
