/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"io/ioutil"
	"path/filepath"
)

var noInteractionBuild bool

// unrealCmd represents the unreal command
var unrealCmd = &cobra.Command{
	Use:   "unreal",
	Short: "Unreal Engine Commands",
	Run: func(cmd *cobra.Command, args []string) {
		err := cmd.Help()
		if err != nil {
			return
		}
	},
}

// unrealBuildCmd represents the unreal command
var unrealBuildCmd = &cobra.Command{
	Use:   "build",
	Short: "Run RunUAT.bat file easly",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		uProjectFile, _ := filepath.Abs(args[0])
		_, errFile := ioutil.ReadFile(uProjectFile)
		if errFile != nil {
			rootCmd.PrintErrln(errFile)
			return
		}
		uProjectDir := filepath.Dir(uProjectFile)
		UEPath := viper.GetString("uepath")
		RunUAT := UEPath + "\\Engine\\Build\\BatchFiles\\RunUAT.bat"
		_, errFileRunUAT := ioutil.ReadFile(RunUAT)
		if errFileRunUAT != nil {
			rootCmd.PrintErrln(errFileRunUAT)
			return
		}
		buildCommand := RunUAT + " BuildCookRun -utf8output -platform=Win64 -clientconfig=Development -serverconfig=Development -project=" + uProjectFile + " -noP4 -nodebuginfo -allmaps -cook -build -stage -prereqs -pak -archive -archivedirectory=" + filepath.Join(uProjectDir, "Build")
		if !noInteractionBuild {
			utils.Info(buildCommand)
			proceed := YesNo(YesNoParameters{YesNoLabel: "Do you want to proceed?"})
			if !proceed {
				return
			}
		}
		_, _ = ExecutePowershell(buildCommand)
	},
}

// unrealSetUePathCmd represents the unreal command
var unrealSetUePathCmd = &cobra.Command{
	Use:   "set-path",
	Short: "Set path of unreal engine installation",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := viperSetSave("uepath", args[0])
		if err != nil {
			return
		}
	},
}

// unrealGetUePathCmd represents the unreal command
var unrealGetUePathCmd = &cobra.Command{
	Use:   "get-path",
	Short: "Get path of unreal engine installation",
	Run: func(cmd *cobra.Command, args []string) {
		path := viper.GetString("uepath")
		if path != "" {
			utils.Succ(path)
		} else {
			utils.Warn("UE path has not been set yet")
		}
	},
}

func init() {
	rootCmd.AddCommand(unrealCmd)
	unrealCmd.AddCommand(unrealBuildCmd)
	unrealCmd.AddCommand(unrealSetUePathCmd)
	unrealCmd.AddCommand(unrealGetUePathCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// unrealCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	unrealBuildCmd.Flags().BoolVarP(&noInteractionBuild, "non-interactive", "y", false, "Ask before execution")
}
