/*
Copyright © 2022 XPOL <paolofalomo@gmail.com>
License Restrictions
*/
package cmd

import (
	"altercode/xpol/utils"
	"context"
	"errors"
	"github.com/PullRequestInc/go-gpt3"
	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"io/ioutil"
	"path/filepath"
	"strings"
)

func openAiClient() (gpt3.Client, context.Context, error) {
	apiKey := viper.GetString("openaikey")

	if len(apiKey) == 0 {
		utils.Warn("Missing api key, use set-api-key subcommand")
		return nil, nil, errors.New("NOAPIKEY")
	}
	ctx := context.Background()
	client := gpt3.NewClient(apiKey)

	return client, ctx, nil
}

// aiCmd represents the ai command
var aiCmd = &cobra.Command{
	Use:   "ai",
	Short: "XPOL Artificial intelligence",
	Long:  `OpenAI stuff`,
	Run: func(cmd *cobra.Command, args []string) {
		client, ctx, aiClientErr := openAiClient()
		if aiClientErr != nil {
			return
		}

		promptReturn := ""
		if len(args) > 0 {
			promptReturn = strings.Join(args, " ")
		} else {

			promptKey := promptui.Prompt{
				Label:       "Chiedimi qualunque cosa",
				HideEntered: true,
			}
			promptReturn, _ = promptKey.Run()
		}
		if len(promptReturn) > 0 {
			err := client.CompletionStreamWithEngine(ctx, "text-davinci-003", gpt3.CompletionRequest{
				Prompt:      []string{promptReturn},
				Temperature: nil,
				Echo:        false,
				MaxTokens:   gpt3.IntPtr(512),
			}, func(response *gpt3.CompletionResponse) {
				cmd.Print(color.New(color.Italic, color.FgGreen).Sprintf(response.Choices[0].Text))
			})
			if err != nil {
				utils.Err(err)
				return
			}
		}
	},
}

var aiSetApiKeyCmd = &cobra.Command{
	Use:   "set-api-key",
	Short: "Set OpenAI API Key",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := viperSetSave("openaikey", args[0])
		if err != nil {
			return
		}
		utils.Succ("Done!")
	},
}

var aiDebugFileCmd = &cobra.Command{
	Use:   "debug-file",
	Short: "Debug a given file",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		client, ctx, aiClientErr := openAiClient()
		if aiClientErr != nil {
			return
		}
		fileAbs, _ := filepath.Abs(args[0])
		fileContent, fileErr := ioutil.ReadFile(fileAbs)
		if fileErr != nil {
			utils.Err(fileErr)
			return
		}
		utils.Italic("Sono presenti bug nel File: " + fileAbs + " ?\n")
		utils.Bold(string(fileContent))
		err := client.CompletionStreamWithEngine(ctx, "text-davinci-003", gpt3.CompletionRequest{
			Prompt:      []string{"Questo script ha dei bug? Non ripetere il codice che ti mando nella risposta, Spiegami gli errori indicando la riga in cui si trovano." + string(fileContent)},
			Temperature: gpt3.Float32Ptr(0),
			Echo:        false,
			MaxTokens:   gpt3.IntPtr(512),
		}, func(response *gpt3.CompletionResponse) {
			cmd.Print(color.New(color.Italic, color.FgGreen).Sprintf(response.Choices[0].Text))
		})
		if err != nil {
			utils.Err(err)
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(aiCmd)
	aiCmd.AddCommand(aiSetApiKeyCmd)
	aiCmd.AddCommand(aiDebugFileCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// aiCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// aiCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
