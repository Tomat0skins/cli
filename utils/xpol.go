package utils

//GENERAL UTILS
import (
	"fmt"
	"github.com/fatih/color"
	"github.com/mitchellh/go-homedir"
	"os"
)

func XPOLDir() string {
	homeDir, _ := homedir.Dir()
	xpolDir := homeDir + "/.xpol"
	_ = os.MkdirAll(xpolDir, os.ModePerm)
	return xpolDir
}

func XPOLDirFile(path string) string {
	return XPOLDir() + "/" + path
}

func HeadLine(line string) {
	fmt.Println(color.New(color.Bold).Sprintf("===== " + line + " ====="))
}
func SubHeadLine(line string) {
	fmt.Println(color.New(color.Bold).Sprintf("---- " + line + " ----"))
}
func Succ(line string) {
	fmt.Println(color.New(color.FgGreen).Sprintf(line))
}
func Info(line string) {
	fmt.Println(color.New(color.FgBlue).Sprintf(line))
}
func Italic(line string) {
	fmt.Println(color.New(color.Italic).Sprintf(line))
}
func Bold(line string) {
	fmt.Println(color.New(color.Bold).Sprintf(line))
}
func KeyVal(key string, value string) {
	fmt.Println(key + ": " + color.New(color.Bold).Sprintf(value))
}
func SuccKeyVal(key string, value string) {
	KeyVal(key, color.New(color.FgGreen).Sprintf(value))
}
func Warn(line string) {
	fmt.Println(color.New(color.FgYellow).Sprintf(line))
}
func Err(err error) {
	fmt.Println(color.New(color.FgRed).Sprintf(err.Error()))
}
