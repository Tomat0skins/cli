package utils

func GetProjects() (*ProjectsResponse, error) {
	req, err := NewProjectsRequest(HasuraGraphQlServerURL)
	if err != nil {
		return nil, err
	}

	req.Request = GetRequestWithAuth(req.Request)

	return req.Execute(HasuraGraphQLClient().Client)
}
